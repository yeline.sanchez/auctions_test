<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Page\InicController@index');
Route::get('/articles', 'Page\ArticlesController@index');
Route::get('/articles/show', 'Page\ArticlesController@show');

Route::get('/login', 'Security\LoginController@index')->name('login');
Route::post('/login', 'Security\LoginController@login')->name('login_post');

Route::get('/articles/{id}', function ($id) {
    return view('page.item')->with('id', $id);
});

Route::get('/search/{text}', function ($text) {
        return view('home')->with('terms', $text);
});


Route::group(['middleware'=>['auth']],function(){


    Route::get('logout', function ()
    {
        auth()->logout();
        Session()->flush();

        return Redirect::to('/');
    })->name('logout');


    Route::group(['middleware' => ['Regular', 'Administrador']], function () {

        Route::get('/home', 'Page\InicController@index')->name('home');

        Route::namespace('Page')->group(function () {
            Route::post('/articles/bid', 'BidsController@store');
            Route::post('/articles/bid/update', 'BidsController@update');
        });

        Route::namespace('User')->group(function () {

            Route::get('/user/configure', 'UserController@index');
            Route::post('/user/configure', 'UserController@store');
            Route::get('/user', 'UserController@show');

            Route::post('/user/configure/autobid', 'AutoBidsController@store');
            Route::post('/user/configure/autobid/disable', 'AutoBidsController@destroy');
        });

    });

    Route::group(['middleware' => ['Administrador']], function () {

    });


});
