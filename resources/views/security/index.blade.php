@extends('layouts.app')
@section('content')

<section class="login py-5 border-top-1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-8 align-item-center">
                <div class="border">
                    <h3 class="bg-gray p-4">Login Now</h3>
                    <form method="POST" action="{{ route('login_post')}}">
                        @csrf
                        <fieldset class="p-4">

                            <div class="input-group mb-3 {{$errors->has('login' ? 'is-invalid' : '')}}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="icon-user"></i>
                                    </span>
                                </div>
                                <input type="text" value="{{old('name')}}" name="name" id="name" class="form-control" placeholder="Username" autocomplete="off">
                                {!!$errors->first('name','<span class="invalid-feedback d-block">:message</span>')!!}
                            </div>

                            <div class="input-group mb-4 {{$errors->has('password' ? 'is-invalid' : '')}}">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                    <i class="icon-lock"></i>
                                    </span>
                                </div>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                                {!!$errors->first('password','<span class="invalid-feedback d-block">:message</span>')!!}
                            </div>
                            <button type="submit" class="d-block py-3 px-5 bg-primary text-white border-0 rounded font-weight-bold mt-3">Log in</button>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>

@endsection
