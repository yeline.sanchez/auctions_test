<!DOCTYPE html>
<html lang="en">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Auctions Online</title>

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="{{ asset('css/plantilla.css') }}">


</head>

<body class="body-wrapper">


<div id="app" >
	@include('layouts.header')
	@include('layouts.search')

    @yield('content')


</div>

<!--============================
=            Footer            =
=============================-->

<footer class="footer section section-sm">

</footer>

<!-- JAVASCRIPTS -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/plantilla.js') }}"></script>

</body>

</html>
