@extends('layouts.app')
@section('content')


<div class="container">
    <div class="row justify-content-center">
        @auth
            <item-component :id="{{$id}}" :userid="{{ Auth::user()->id }}" ></item-component>
        @endauth

        @guest
            <item-component :id="{{$id}}" ></item-component>
        @endguest

    </div>
</div>



@endsection