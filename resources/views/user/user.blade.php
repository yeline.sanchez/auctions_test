@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        @auth
            <user-component :userid="{{ Auth::user()->id }}"></user-component>
        @endauth
    </div>
</div>

@endsection