const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
  'resources/theme/plugins/bootstrap/dist/css/bootstrap.css',
  'resources/theme/plugins/bootstrap/dist/css/bootstrap-slider.css',  
  'resources/theme/plugins/font-awesome/css/font-awesome.min.css',  
  'resources/theme/plugins/slick-carousel/slick/slick.css',
  'resources/theme/plugins/slick-carousel/slick/slick-theme.css',  
  'resources/theme/plugins/fancybox/jquery.fancybox.pack.css',    
  'resources/theme/css/style.css',
], 'public/css/plantilla.css')
.scripts([    
  'resources/theme/plugins/bootstrap/dist/js/popper.min.js',
  'resources/theme/plugins/bootstrap/dist/js/bootstrap.min.js',
  'resources/theme/plugins/bootstrap/dist/js/bootstrap-slider.js',
  'resources/theme/plugins/tether/js/tether.min.js',
  'resources/theme/plugins/raty/jquery.raty-fa.js',
  'resources/theme/plugins/slick-carousel/slick/slick.min.js',    
  'resources/theme/plugins/fancybox/jquery.fancybox.pack.js',
  'resources/theme/plugins/smoothscroll/SmoothScroll.min.js',
  'resources/theme/js/script.js',
], 'public/js/plantilla.js')
 .copyDirectory('resources/theme/img', 'public/storage')
 .copyDirectory('resources/theme/plugins/font-awesome/fonts', 'public/fonts')
 .js('resources/js/app.js', 'public/js') 
 .sass('resources/sass/app.scss', 'public/css'); 
