<?php

namespace App\Listeners;

use App\Events\BidsUpdate;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Models\Auto_Bids;
use App\Models\Bid;
use App\User;
use Illuminate\Support\Facades\Log;

class AutoBidsUpdate
{
    
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BidsUpdate  $event
     * @return void
     */
    public function handle(BidsUpdate $event)
    {   
        $auto = Auto_Bids::where('article_id', $event->bid->article_id)->first(); 
        
        if (!empty($auto) && !is_null($auto) && $auto->user_id !== $event->bid->user_id){       
            
            Log::info('An informational message:' . $auto->user_id . ' // ' .$event->bid->user_id);

            $bid = Bid::find($event->bid->id);
            $bid->max_bid = $event->bid->max_bid + 1  ;
            $bid->user_id = $auto->user_id;
            $bid->history = $event->bid->history + 1;
            $bid->save();

            $user = User::where('id', $auto->user_id)->first();           

            if ($user->bid_amount > 1){
                $user->bid_amount = $user->bid_amount - 1;
                $user->save();
            } 
            else{
                $auto->delete();
            } 
        } 
        return false;
                    
    }
}
