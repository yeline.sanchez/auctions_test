<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lots extends Model
{
    protected $table = 'lots';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'description', 'starting_price', 'deadline', 'category_id', 'created_at', 'updated_at'];

    public function bids()
    {
        return $this->hasOne('App\Models\Bids');
    }

    public function categories()
    {
        return $this->hasOne('App\Models\Categories');
    }

    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }
}
