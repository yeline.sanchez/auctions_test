<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Auto_Bids extends Model
{
    //
    protected $table = 'auto_bids';

    protected $primaryKey = 'id';

    protected $fillable = ['lot_id', 'user_id'];

    public $timestamps = false;
}
