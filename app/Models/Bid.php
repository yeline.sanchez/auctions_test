<?php

namespace App\Models;
use App\Events\BidsUpdate;

use Illuminate\Database\Eloquent\Model;

class Bid extends Model
{
    protected $table = 'bids';

    protected $primaryKey = 'id';

    protected $fillable = ['article_id', 'user_id', 'max_bid', 'history', 'created_at', 'updated_at'];

    protected $dispatchesEvents = [
        'updated' => BidsUpdate::class,
        'created' => BidsUpdate::class,
    ];
   
}
