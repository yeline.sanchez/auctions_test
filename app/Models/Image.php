<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $table = 'images';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'lot_id', 'data'];

    public $timestamps = false;


}
