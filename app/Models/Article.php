<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $primaryKey = 'id';

    protected $fillable = ['title', 'description', 'starting_price', 'deadline', 'image', 'created_at', 'updated_at'];
    
    public function bids()
    {
        return $this->hasOne('App\Models\Bids');
    } 

    public function images()
    {
        return $this->hasMany('App\Models\Image');
    }    
       
}
