<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class InicController extends Controller
{
    //
    public function index()
    {
        return view('home')->with('terms', '');
    }
}
