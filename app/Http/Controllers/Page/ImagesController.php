<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Image;
use Illuminate\Support\Facades\DB;

class ImagesController extends Controller
{
    //
    public function show(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
                
        $image = Image::find($request->id);

        return ['image' => $image];
    }
    
}
