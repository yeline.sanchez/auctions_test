<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Bid;
use App\Events\BidsUpdate;
use Illuminate\Support\Facades\DB;

class BidsController extends Controller
{    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        if (!$request->ajax()) return redirect('/'); 

        if (Auth::check()) {
            $user = Auth::user()->id;            
            
            DB::beginTransaction();

            $bid = new Bid;
            $bid->user_id = $user;
            $bid->article_id = $request->article;
            $bid->max_bid = $request->amount;
            $bid->save();   
            
            DB::commit();

            return ['item' => $bid] ;       
        }
        else
          return route('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        if (!$request->ajax()) return redirect('/'); 

        if (Auth::check()) {
            $user = Auth::user()->id; 
            
            DB::beginTransaction();
            
                $bid = Bid::where('article_id', $request->article)->lockForUpdate()->first(); 

                $bid->max_bid = $request->amount;
                $bid->user_id = $user;
                $bid->history = $bid->history + 1;
                $bid->save(); 

            DB::commit();
        
            return ['item' => $bid] ;
        }
        else
          return route('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
