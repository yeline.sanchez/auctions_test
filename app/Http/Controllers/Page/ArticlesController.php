<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Article;
use Illuminate\Support\Facades\DB;


class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');  

        $query = DB::table('articles')
                            ->select('articles.id', 'articles.starting_price', 'articles.deadline', 'articles.title', 'articles.image', DB::raw('COALESCE(bids.max_bid, 0) as max_bid'), DB::raw('COALESCE(bids.history, 0) as history'))                                                
                            ->leftJoin('bids', function($join) { 
                                $join->on('articles.id', '=', 'bids.article_id'); 
                            });

        if (!empty($request->search) or !is_null($request->search)){
            $search = strtolower($request->search);  
            $query->where(DB::raw('lower(title)'), 'like', '%'. $search . '%')
                  ->orWhere(DB::raw('lower(description)'), 'like', '%'. $search . '%');                                       
        }        

        $query->orderBy($request->sort, $request->order);
        $articles= $query->paginate(12); 

        return [
            'pagination' => [
                        'total'        => $articles->total(),
                        'current_page' => $articles->currentPage(),
                        'per_page'     => $articles->perPage(),
                        'last_page'    => $articles->lastPage(),
                        'from'         => $articles->firstItem(),
                        'to'           => $articles->lastItem()
            ],
            'articles' => $articles           
        ];        
    }


    public function show(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
                
        $item = DB::select('select a.*, COALESCE(b.auto, 0) as auto from (select articles.*, COALESCE(s.max_bid, 0) as max_bid, COALESCE(s.history, 0) as history, s.user_id
                                             from articles left join (select max_bid, history, user_id, article_id from bids where article_id = :id) as s  on articles.id = s.article_id
                                    where articles.id = :id) as a left join (select user_id as auto, article_id from auto_bids ) as b on a.id = b.article_id', ['id' => $request->id]);                            
       
        $images = Article::find($request->id)->images()->get();

        return ['item' => $item,
                'images' => $images];
    } 
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
}
