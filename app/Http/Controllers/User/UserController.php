<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    //
    public function index()
    {
        //
        return view('user.user');
    }

    public function store(Request $request)
    {        
        if (!$request->ajax()) return redirect('/'); 
        
        $user = User::find($request->id);           
        $user->bid_amount = $request->amount;           
        $user->save();    
        return ['user' => $user] ;       
        
    }

    public function show(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $user = User::find(Auth::user()->id); 

        return ['user' => $user] ;  

    }
}
