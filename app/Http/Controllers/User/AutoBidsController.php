<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Auto_Bids;

class AutoBidsController extends Controller
{
    //

    public function store(Request $request)
    {        
        if (!$request->ajax()) return redirect('/'); 

        if (Auth::check()) {
            $user = Auth::user()->id; 

            $auto = new Auto_Bids;
            $auto->user_id = $user;
            $auto->article_id = $request->id;
            $auto->save();    

            return ['autobid' => $auto] ;  
        }     
        
    }

    public function destroy(Request $request)
    {        
        $bids = Auto_Bids::where('article_id', $request->id);
        $bids->delete();
    }
}
