<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Roles::create([
            'role' => 'Administrador'
        ]);

        Roles::create([
            'role' => 'Regular'
        ]);
    }
}
