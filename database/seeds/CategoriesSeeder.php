<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'category' => 'Muebles'
        ]);

        Category::create([
            'category' => 'Pinturas Decorativas'
        ]);

        Category::create([
            'category' => 'Relojes'
        ]);

        Category::create([
            'category' => 'Alhajas'
        ]);
    }
}
