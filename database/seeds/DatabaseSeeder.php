<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            UserSeeder::class,
            CategoriesTableSeeder::class,
            LotsTableSeeder::class,
            BidsTableSeeder::class,
            ImageSeeder::class,
        ]);
    }
}
